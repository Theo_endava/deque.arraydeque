package com.company;

import java.time.LocalDate;
//import java.util.ArrayDeque;

public class Student {
    private String numarStudent;
//    private String name;
//    private LocalDate dateOfBirth;
//    private String details;

//  public Student(String name, LocalDate dateOfBirth, String details) {
//        this.name = name;
//        this.dateOfBirth = dateOfBirth;
//        this.details = details;

    public Student(String numarStudent) {
        this.numarStudent = numarStudent;
    }

//    public String getName() {
//        return name;
//    }
//
//    public LocalDate getDateOfBirth() {
//        return dateOfBirth;
//    }
//
//    public String getDetails() {
//        return details;
//    }

//    @Override
//    public String toString() {
//        return "Student{" + "name='" + name + '\'' + ", dateOfBirth=" + dateOfBirth + ", details='" + details + '\'' + '}';
//    }


    @Override
    public String toString() {
        return numarStudent;
    }

    public static void main(String[] args) {

        //Student student1 = new Student("Teodor", LocalDate.now(), "Details");
        //Student student2 = new Student("Teaca", LocalDate.now(), "Details");

        Student Studnet1 = new Student("Student1");
        Student Studnet2 = new Student("Student2");
        Student Studnet3 = new Student("Student3");
        Student Studnet4 = new Student("Student4");

        ArrayDequeImplement array = new ArrayDequeImplement();

        array.addFirst(Studnet1);
        System.out.println("addFirst"+array);
        array.addLast(Studnet2);
        array.addLast(Studnet3);
        System.out.println("addLast"+array);
        array.removeLast();
        System.out.println("remuveLast"+array);
        array.removeFirst();
        System.out.println("removeFirst"+array);
        array.add(Studnet3);
        System.out.println("add"+array);
        System.out.println("peek"+array.peek());
        array.offerFirst(Studnet4);
        System.out.println("offerFirst"+array);
        array.offerLast(Studnet3);
        System.out.println("offerLast"+array);
        System.out.println("getFirst"+array.getFirst());
        System.out.println("getLast"+array.getLast());
        array.removeFirstOccurrence(Studnet3);
        System.out.println("removeFirstOccurrence"+array);
        System.out.println("size"+array.size());

    }

}

