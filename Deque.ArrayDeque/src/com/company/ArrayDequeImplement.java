package com.company;

import java.util.*;

public class ArrayDequeImplement implements Deque<Student> {

    private Student[] arrayDequeWithStudents;

    public int firstElement = 0;
    private int lastElement = 0;

    private static final int DEFAULT_ARRAY_INITIAL_SIZE = 5;

    public String toString() {
        return Arrays.toString(arrayDequeWithStudents);
    }

    public ArrayDequeImplement() {
        arrayDequeWithStudents = new Student[DEFAULT_ARRAY_INITIAL_SIZE];
    }

    public ArrayDequeImplement(int size) {
        arrayDequeWithStudents = new Student[size];
    }

    @Override
    public void addFirst(Student student) {
        if (student == null) {
            throw new NullPointerException();
        }
        if (isEmpty()) {
            arrayDequeWithStudents[firstElement] = student;
            lastElement++;
        } else if (firstElement > 0) {
            arrayDequeWithStudents[--firstElement] = student;
        } else {
            if (lastElement > arrayDequeWithStudents.length - 1) {
                resize();
            }
            shiftRight();
            arrayDequeWithStudents[0] = student;
            lastElement++;
        }
    }

    @Override
    public void addLast(Student student) {
        if (student == null) {
            throw new NullPointerException();
        }
        if (lastElement > arrayDequeWithStudents.length - 1) {
            resize();
        }
        arrayDequeWithStudents[lastElement] = student;
        lastElement++;
    }

    @Override
    public boolean offerFirst(Student student) {
        addFirst(student);
        return true;
    }

    @Override
    public boolean offerLast(Student student) {
        addLast(student);
        return true;
    }

    @Override
    public Student removeFirst() {
        arrayDequeWithStudents[firstElement] = null;
        shiftLeft();
        return null;
    }

    @Override
    public Student removeLast() {
        Student s = arrayDequeWithStudents[lastElement - 1];
        arrayDequeWithStudents[--lastElement] = null;
        return s;
    }

    @Override
    public Student pollFirst() {
        if (isEmpty()) {
            return null;
        }
        Student pollFirst = arrayDequeWithStudents[firstElement];
        arrayDequeWithStudents[firstElement] = null;
        shiftLeft();
        return pollFirst;
    }

    @Override
    public Student pollLast() {
        if (isEmpty()) {
            return null;
        }
        Student pollLast = arrayDequeWithStudents[lastElement - 1];
        arrayDequeWithStudents[--lastElement] = null;
        return pollLast;
    }

    @Override
    public Student getFirst() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return arrayDequeWithStudents[firstElement];
    }

    @Override
    public Student getLast() {
        if (isEmpty())
            throw new NoSuchElementException();
        return arrayDequeWithStudents[lastElement - 1];
    }

    @Override
    public Student peekFirst() {
        if (isEmpty()) {
            return null;
        }
        return arrayDequeWithStudents[firstElement];
    }

    @Override
    public Student peekLast() {
        if (isEmpty()) {
            return null;
        }
        return arrayDequeWithStudents[lastElement];
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        if (o == null) throw new NullPointerException();
        if (!(o instanceof Student)) throw new ClassCastException();
        for (int i = 0; i < arrayDequeWithStudents.length; i++) {

            if (o.equals(arrayDequeWithStudents[i])) {
                arrayDequeWithStudents[i] = null;
                for (int j = i; j < lastElement - 1; j++) {
                    arrayDequeWithStudents[j] = arrayDequeWithStudents[j + 1];
                    if (j == lastElement - 2) {
                        arrayDequeWithStudents[j + 1] = null;
                    }
                }
                lastElement--;
                return true;
            }
        }
        return true;
    }


    @Override
    public boolean removeLastOccurrence(Object o) {
        int cont = lastElement;
        if (o == null) throw new NullPointerException();
        if (!(o instanceof Student)) throw new ClassCastException();
        for (int i = arrayDequeWithStudents.length - 1; i > 0; i--) {
            cont--;
            if (o == arrayDequeWithStudents[i]) {
                arrayDequeWithStudents[i] = null;
                for (int j = cont; j < -1; j++) {
                    arrayDequeWithStudents[j] = arrayDequeWithStudents[j + 1];
                    if (j == lastElement - 2) {
                        arrayDequeWithStudents[j + 1] = null;
                        lastElement--;
                    }
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean add(Student student) {
        addLast(student);
        return false;
    }

    @Override
    public boolean offer(Student student) {
        offerLast(student);
        return false;
    }

    @Override
    public Student remove() {
        return removeFirst();
    }

    @Override
    public Student poll() {
        Student s = arrayDequeWithStudents[lastElement];
        return remove();
    }

    @Override
    public Student element() {
        return getFirst();
    }

    @Override
    public Student peek() {
        return pollFirst();
    }

    @Override
    public void push(Student student) {
        addFirst(student);
    }

    @Override
    public Student pop() {
        return removeFirst();
    }

    @Override
    public boolean remove(Object o) {
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!contains(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends Student> c) {
        for (Student student : c) {
            add(student);
            return true;
        }
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for (Object o : c) {
            remove(o);
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        if (isEmpty()) {
            throw new NullPointerException();
        }
        boolean changed;
        Student[] newStudents = new Student[arrayDequeWithStudents.length];
        int i = 0;
        for (Student student : arrayDequeWithStudents) {
            if (c.contains(student)) {
                newStudents[i] = student;
                i++;
            }
        }
        changed = (i != this.size());
        lastElement = i + 1;
        arrayDequeWithStudents = newStudents;
        return changed;
    }

    @Override
    public void clear() {
        for (int i = 0; i < lastElement; i++) {
            arrayDequeWithStudents[i] = null;
        }
    }

    @Override
    public boolean contains(Object o) {
        if (!(o instanceof Student)) {
            throw new ClassCastException();
        }
        for (int i = 0; i < lastElement; i++) {
            if (o.equals(arrayDequeWithStudents[i])) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return lastElement - firstElement + 1;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public Iterator<Student> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public Iterator<Student> descendingIterator() {
        return null;
    }

    public void shiftRight() {
        for (int i = arrayDequeWithStudents.length - 1; i > 0; i--) {
            arrayDequeWithStudents[i] = arrayDequeWithStudents[i - 1];
        }


    }

    public void shiftLeft() {
        for (int i = 0; i < arrayDequeWithStudents.length - 1; i++) {
            arrayDequeWithStudents[i] = arrayDequeWithStudents[i + 1];
            if (i == lastElement - 2) {
                arrayDequeWithStudents[i + 1] = null;
                lastElement--;
            }
        }
    }

    public void resize() {
        int newSize = arrayDequeWithStudents.length * 2;
        Student[] newArrayDequeStudents = new Student[newSize];

        for (int i = 0; i < arrayDequeWithStudents.length; i++) {
            newArrayDequeStudents[i] = arrayDequeWithStudents[i];
        }
        arrayDequeWithStudents = newArrayDequeStudents;
    }

}
